FROM python:3.10-slim-buster
WORKDIR /src

ENV PYTHONPATH=/src/app

ADD requirements.txt /src/requirements.txt

RUN pip install --upgrade -r requirements.txt
RUN apt update
RUN apt install sqlite3

EXPOSE 8080

COPY ./ /src

CMD ["uvicorn", "app.app:app", "--host", "0.0.0.0", "--port", "8080"]

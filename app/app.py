from fastapi import (
    BackgroundTasks,
    Body,
    FastAPI
)
from app.persistence.database import (
    are_orders_on_week,
    disburse_calculation,
    get_merchant,
    get_persisted_disburses
)
from app.schemas.schemas import DisburseRequestSchema
from app.exceptions import MerchantNotFoundException

app = FastAPI()


@app.get("/status")
def home():
    """
    This Endpoint retrieves 200 status if the services is up and running
    """
    return {"message": "SeQura service is running"}


@app.get("/disburse")
async def disburse(background_tasks: BackgroundTasks,
                   disburse_request: DisburseRequestSchema = Body(...)):
    """
    This Endpoint retrieves from db the disburses for the requested week
    and year. In case that the requested disburse are not present on DB,
    a background calculation task is being triggered
    """
    if disburse_request.merchant_id:
        try:
            get_merchant(disburse_request.merchant_id)
        except MerchantNotFoundException:
            return {"message": "invalid merchant id"}

    if not are_orders_on_week(
            disburse_request.merchant_id,
            disburse_request.week
    ):
        return {"message": "there are no order for the request"}

    disburses = get_persisted_disburses(
        merchant_id=disburse_request.merchant_id,
        week=disburse_request.week,
        year=disburse_request.year
    )
    if disburses:
        return {'disburses': [{"merchant_id": d.merchant_id,
                               "amount": round(d.amount, 2),
                               "week": d.week,
                               "year": d.year}
                              for d in disburses]}
    background_tasks.add_task(
        disburse_calculation,
        disburse_request.week,
        disburse_request.year,
        disburse_request.merchant_id
    )
    return {"message":
            "Disburses calculation is being performed on background"}

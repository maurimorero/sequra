from sqlalchemy import (
    Column,
    Boolean,
    Float,
    Integer,
    String,
    ForeignKey
)
from sqlalchemy.orm import relationship
from sqlalchemy_utils import EmailType
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Merchant(Base):
    __tablename__ = "merchants"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    email = Column(EmailType)
    cif = Column(String)
    orders = relationship("Order")


class Shopper(Base):
    __tablename__ = "shoppers"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    email = Column(EmailType)
    nif = Column(String)
    orders = relationship("Order")


class Order(Base):
    __tablename__ = "orders"

    id = Column(Integer, primary_key=True)
    amount = Column(Float)
    week = Column(Integer)
    year = Column(Integer)
    completed = Column(Boolean)
    merchant_id = Column(Integer, ForeignKey("merchants.id"))
    shopper_id = Column(Integer, ForeignKey("shoppers.id"))


class Disburse(Base):
    __tablename__ = "disburses"

    amount = Column(Float)
    bulk_creation = Column(Boolean, primary_key=True)
    week = Column(Integer, primary_key=True)
    year = Column(Integer, primary_key=True)
    merchant_id = Column(Integer, ForeignKey("merchants.id"), primary_key=True)

import os
from dotenv import load_dotenv

from sqlalchemy import create_engine, select, case, and_
from sqlalchemy.sql import func
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from app.models.models import Merchant, Order, Disburse
from app.exceptions import MerchantNotFoundException
from typing import List, Union
from app.constants import LOWER_LEVEL_FEE, MID_LEVEL_FEE, UPPER_LEVEL_FEE

load_dotenv()

engine = create_engine(os.getenv('DB_DETAILS'))


def get_merchant(merchant_id: int) -> Merchant:
    """
    This method takes care of searching on DB a specific merchang.
    In case it is not found, and MerchantNotFoundException is raised
    """
    with Session(engine) as session:
        merchant = session.query(Merchant).filter(
            Merchant.id == merchant_id
        ).first()
    if not merchant:
        raise MerchantNotFoundException
    return merchant


def are_orders_on_week(merchant_id: int, week: int) -> bool:
    """
    This method takes care of checking if there are order for a
    specific week
    """
    with Session(engine) as session:
        if merchant_id:
            orders = session.query(Order).filter(
                Order.merchant_id == merchant_id,
                Order.week == week,
            ).first()
        else:
            orders = session.query(Order).filter(
                Order.week == week,
            ).first()
    if not orders:
        return False
    return True


def _get_and_statement(merchant_id: int, week: int, year: int):
    """
    This protected function takes care of returning the appropriated AND
    statement for the SQL Query, taking into account if the query is for
    one or all merchants
    """
    return and_(
                    (Order.merchant_id == merchant_id),
                    (Order.year == year),
                    (Order.week == week),
                    Order.completed
                ) if merchant_id else and_(
                    (Order.year == year),
                    (Order.week == week),
                    Order.completed
                )


def get_persisted_disburses(
        merchant_id: Union[int, None],
        week: int,
        year: int
) -> List[Disburse]:
    """
    Basically, this function retrieves from db the disbursed based on
    the merchant_id, week and year provided. If merchant id is None, then
    all disburses are provided
    """
    with Session(engine) as session:
        if merchant_id:
            disburses = session.query(Disburse).filter(
                Disburse.merchant_id == merchant_id,
                Disburse.week == week,
                Disburse.year == year
            ).all()
        else:
            disburses = session.query(Disburse).filter(
                Disburse.week == week,
                Disburse.year == year,
                Disburse.bulk_creation
            ).all()
    return disburses


def disburse_calculation(week: int, year: int, merchant_id: int = None):
    """
    This method executes an SQL Query for disburse calculation, and then
    it takes care of storing them into the DB.
    """
    query = select(
        [
            Order.merchant_id.label("merchant_id"),
            case(
                [
                    (
                        Order.amount < 50,
                        func.sum(Order.amount -
                                 (Order.amount * LOWER_LEVEL_FEE))
                    ),
                    (
                        and_((Order.amount >= 50), (Order.amount <= 300)),
                        func.sum(Order.amount -
                                 (Order.amount * MID_LEVEL_FEE))
                    )
                ],
                else_=(func.sum(Order.amount -
                                (Order.amount * UPPER_LEVEL_FEE))),
            ).label("disburse")
        ]
    ).select_from(Order).where(_get_and_statement(merchant_id, week, year)
                               ).group_by(
        Order.merchant_id
    )
    with engine.connect() as connection:
        result = connection.execute(query)
        disburses = result.mappings().all()

    with Session(engine) as session:
        for disburse in disburses:
            session.begin()
            try:
                new_disburse = Disburse(
                    amount=disburse['disburse'],
                    week=week,
                    year=year,
                    merchant_id=disburse['merchant_id'],
                    bulk_creation=True if not merchant_id else False
                )
                session.add(new_disburse)
                session.commit()
            except IntegrityError:
                session.rollback()

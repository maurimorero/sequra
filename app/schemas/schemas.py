from pydantic import BaseModel, Field
import datetime
from typing import Optional

current_year = datetime.date.today().strftime("%Y")


class DisburseRequestSchema(BaseModel):
    merchant_id: Optional[int] = None
    week: int = Field(gt=0, le=53)
    year: int = Field(gt=2000, le=int(current_year))

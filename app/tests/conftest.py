import pytest
from app.persistence.database import engine
from sqlalchemy.orm import Session
from app.models.models import Disburse


@pytest.fixture
def clean_resources():
    yield
    with Session(engine) as session:
        session.query(Disburse).delete()
        session.commit()

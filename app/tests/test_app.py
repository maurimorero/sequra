import pytest
from fastapi.testclient import TestClient
from app.tests.conftest import clean_resources
from app.app import app
from app.utils import get_week_and_year


def test_home_endpoint():
    with TestClient(app) as client:
        response = client.get('/status')
        assert response.status_code == 200


def test_disburse_endpoint_background_check(clean_resources):
    with TestClient(app) as client:
        response = client.get('/disburse',
                              json={
                                  "merchant_id": 1,
                                  "week": 1,
                                  "year": 2018
                              })
        assert response.status_code == 200
        assert response.json()['message'] == "Disburses calculation " \
                                             "is being performed on " \
                                             "background"
        response = client.get('/disburse',
                              json={
                                  "merchant_id": 1,
                                  "week": 1,
                                  "year": 2018
                              })
        assert response.json()['disburses'][0]['merchant_id'] == 1
        assert response.json()['disburses'][0]['amount'] == 1652.29


def test_disburse_endpoint_multiple_merchants(clean_resources):
    with TestClient(app) as client:
        response = client.get('/disburse',
                              json={
                                  "week": 1,
                                  "year": 2018
                              })
        assert response.status_code == 200
        assert response.json()['message'] == "Disburses calculation " \
                                             "is being performed on " \
                                             "background"
        response = client.get('/disburse',
                              json={
                                  "week": 1,
                                  "year": 2018
                              })
        assert len(response.json()['disburses']) > 1


@pytest.mark.parametrize("merchant_id, "
                         "week, "
                         "year, "
                         "expected_exit_code, "
                         "expected_message", [
    (-1, 1, 2020, 200, "invalid merchant id"),
    (1, 60, 2020, 422, None),
    (1, 1, 0, 422, None),
    (1, -10, 2020, 422, None),
    (1, 10, 2030, 422, None)])
def test_bad_requests(
        merchant_id,
        week,
        year,
        expected_exit_code,
        expected_message
):
    with TestClient(app) as client:
        response = client.get('/disburse',
                              json={
                                  "merchant_id": merchant_id,
                                  "week": week,
                                  "year": year
                              })
        assert response.status_code == expected_exit_code
        if expected_message:
            assert response.json()['message'] == expected_message


def test_invalid_merchant():
    with TestClient(app) as client:
        response = client.get('/disburse',
                              json={
                                  "merchant_id": 150,
                                  "week": 1,
                                  "year": 2018
                              })
        assert response.json()['message'] == "invalid merchant id"


def test_request_with_no_orders():
    with TestClient(app) as client:
        response = client.get('/disburse',
                              json={
                                  "merchant_id": 1,
                                  "week": 50,
                                  "year": 2018
                              })
        assert response.json()['message'] == "there are no order for the request"


def test_get_week_and_year():
    test_date = "03/01/2022 00:00:00"
    week, year = get_week_and_year(test_date)

    assert week == 1
    assert year == 2022

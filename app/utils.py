from datetime import datetime
from typing import Tuple


def get_week_and_year(date: str) -> Tuple:
    date_time_obj = datetime.strptime(date, '%d/%m/%Y %H:%M:%S').isocalendar()
    return date_time_obj.week, date_time_obj.year

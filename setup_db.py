from os import path
from app.models import models
from sqlalchemy import insert
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import json
from app.utils import get_week_and_year
from app.constants import PRD_DB_DETAILS, TEST_DB_DETAILS

engine = create_engine(PRD_DB_DETAILS)
engine_test = create_engine(TEST_DB_DETAILS)

# Useful paths
RESOURCES_PATH = path.join("resources/")
MERCHANT_JSON_PATH = path.join(RESOURCES_PATH, "merchants.json")
SHOPPERS_JSON_PATH = path.join(RESOURCES_PATH, "shoppers.json")
ORDERS_JSON_PATH = path.join(RESOURCES_PATH, "orders.json")

for engine_db in [engine, engine_test]:
    SessionLocal = sessionmaker(
        autocommit=False,
        autoflush=False,
        bind=engine_db
    )
    db = SessionLocal()
    models.Base.metadata.create_all(bind=engine_db)

    with open(MERCHANT_JSON_PATH) as json_file:
        db.execute(insert(models.Merchant), json.load(json_file)['RECORDS'])
        db.commit()

    with open(SHOPPERS_JSON_PATH) as json_file:
        db.execute(insert(models.Shopper), json.load(json_file)['RECORDS'])
        db.commit()

    with open(ORDERS_JSON_PATH) as json_file:
        orders = []
        for order in json.load(json_file)['RECORDS']:
            week, year = get_week_and_year(order['created_at'])
            new_order = models.Order(
                id=order['id'],
                merchant_id=order['merchant_id'],
                shopper_id=order['shopper_id'],
                amount=order['amount'],
                week=week,
                year=year,
                completed=True if order['completed_at'] else False
            )
            orders.append(new_order)
        db.add_all(orders)
        db.commit()
    db.close()
